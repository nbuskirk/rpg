Game = {
  // This defines our grid's size and the size of each of its tiles
 outsideDim: false,
  
  map_grid: {
    width:  38,
    height: 19,
    tile: {
      width:  32,
      height: 32
    }
  },

  // The total width of the game screen. Since our grid takes up the entire screen
  //  this is just the width of a tile times the width of the grid
  width: function() {
    return this.map_grid.width * this.map_grid.tile.width;
  },

  // The total height of the game screen. Since our grid takes up the entire screen
  //  this is just the height of a tile times the height of the grid
  height: function() {
    return this.map_grid.height * this.map_grid.tile.height;
  },
  dimOutside:function(t) {	
  
	var tile = t[0].obj;
	$(this._tContainer).html(tile.desc);
	Crafty('OutdoorTile').each(function() {
		this.alpha = 0;
	})
	Crafty('IndoorTile').each(function() {
		this.alpha = 1;
	})
  },
  dimInside:function(t) {
  
	var tile = t[0].obj;
	$(this._tContainer).html(tile.desc);
	Crafty('IndoorTile').each(function() {
		this.alpha = 0;
	})
	Crafty('OutdoorTile').each(function() {
		this.alpha = 1;
	})
  },
  addInterface: function() {
	this._iContainer = $("<div>Avatar   90g</div>");
	this._tContainer = $("<div>Welcome to Ultima Classic</div>");
	$(Crafty.stage.elem).css({
		'width':((Crafty.viewport.width*2)+10)+'px',
		'height':'auto',
		'overflow':'shown'
	})
	$(this._iContainer).css({
		'width':Crafty.viewport.width+'px',
		'height':Crafty.viewport.height+'px',
		'border':'5px solid white',
		'margin-left':'10px',
		'margin-bottom':'10px',
		'box-sizing':'border-box',
		'float':'left',
		'color':'white',
		'font-family':'FixedSys',
		'font-size':'24px',
		'padding':'10px'
	})
	$(this._tContainer).css({
		'width':'100%',
		'box-sizing':'border-box',
		'clear':'both',
		'color':'white',
		'border':'5px solid white',
		'text-align':'left',
		'padding':'10px',
		'font-size':'22px',
		'font-family':'FixedSys',
		'height':'auto',
	})
	$('Canvas').css({
		'float':'left',
		'position':'relative'
	})
	$(Crafty.stage.elem).append(this._iContainer);
	$(Crafty.stage.elem).append(this._tContainer);
  },
  start: function() {
    Crafty.init(714, 422); //21x13 for now
	Crafty.pixelart(true);
	Crafty.timer.FPS(5);
    Crafty.scene('Loading');
  },
  begin: function() {
	Crafty.audio.play('journey',-1);
	Crafty.viewport.follow(Crafty('PlayerCharacter'),0,0);
	Game.addInterface();
  }
}

$text_css = { 'font-size': '64px', 'font-family': 'Arial', 'color': 'white', 'text-align': 'center' }