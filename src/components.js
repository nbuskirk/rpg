// The Grid component allows an element to be located
//  on a grid of tiles
Crafty.c('Grid', {
  init: function() {
    this.attr({
      w: Game.map_grid.tile.width,
      h: Game.map_grid.tile.height
    })
  },

  // Locate this entity at the given position on the grid
  at: function(x, y) {
    if (x === undefined && y === undefined) {
      return { x: this.x/Game.map_grid.tile.width, y: this.y/Game.map_grid.tile.height }
    } else {
      this.attr({ x: x * Game.map_grid.tile.width, y: y * Game.map_grid.tile.height });
      return this;
    }
  }
});

Crafty.c('ViewportBounded', {
	init: function() {
		this.requires('2D');
	},
	checkOutOfBounds: function(from) {
		if(!this.within(0, 0, Game.width(), Game.height())) {
			this.attr({x: from.x, y: from.y});
		} else {
			this._onTile = true;
		}
	}
});

// An "Actor" is an entity that is drawn in 2D on canvas
//  via our logical coordinate grid
Crafty.c('Actor', {
  init: function() {
    this.requires('2D, Canvas, Grid');
  },
  setSprite:function(spr) {
	this.addComponent(spr);
	return this;
  }	
});
Crafty.c('MouseHighlighter',{
	init:function() {
		this.requires('Mouse, Color')
		.color('none')
		.bind('MouseOver',function() {
			if(Crafty('PlayerCharacter').currentAction != 'none') {
				this.color('rgba(255,0,255,0.65)');
			}
		})
		.bind('MouseOut',function() {
			this.color('none');
		})

	}
});
Crafty.c('Door', {
	init:function() {
		this.requires('Mouse')
		.bind('Click',function() {
			if(Crafty('PlayerCharacter').currentAction === 'Open') {
				$(Game._tContainer).html("Door opened.");
				this.destroy();
			}
			Crafty('PlayerCharacter').currentAction = 'none';
		})
	}
});

Crafty.c('IndoorTile',{
	init:function(){ 
		this.requires('Mouse')
		.bind('Click',function() {
			if(Crafty('PlayerCharacter').currentAction === 'Open') {
				$(Game._tContainer).html("You can't open that!");
			}		
			Crafty('PlayerCharacter').currentAction = 'none';			
		});
	}
})
Crafty.c('OutdoorTile', {
	init:function() {
		this.alpha = 0;
		this.requires('Mouse')
		.bind('Click',function() {
			if(Crafty('PlayerCharacter').currentAction === 'Open') {
				$(Game._tContainer).html("You can't open that!");		
			}	
			Crafty('PlayerCharacter').currentAction = 'none';			
		});
	}
});
Crafty.c('CastleEntrance',{
	init:function() {
		this.requires('Mouse')
		.bind('Click',function() {
			if(Crafty('PlayerCharacter').currentAction === 'Enter') {
				Crafty.scene('Britain');
			}
			Crafty('PlayerCharacter').currentAction = 'none';
		})
	}
})
// This is the player-controlled character
Crafty.c('PlayerCharacter', {
	init: function() {
		this._onTile = true;
		this.currentAction = 'none';
		this.requires('Actor, Fourway, Collision, ViewportBounded,Persist')
		.fourway(32)
		.onHit('Solid', this.stopMovement)
		.onHit('CastleEntrance',function(){
			//Crafty.scene('Britain');
		})
		.onHit('IndoorTile', function(e) {
			if(this._onTile === true) {
				Game.dimOutside(e);
				this._onTile = false;
			}
		})
		.onHit('OutdoorTile', function(e) {
			if(this._onTile === true) {
				Game.dimInside(e);
				this._onTile = false;
			}
		})
		.bind('Moved',function(oldPosition){
			this.currentAction = 'none';
			this.checkOutOfBounds(oldPosition);
        })
		.bind('KeyDown',function(e) {
			if(e.key === Crafty.keys.O) {
				this.currentAction = 'Open';
				$(Game._tContainer).html('Open what-?');
			}else if(e.key === Crafty.keys.E) {
				this.currentAction = 'Enter';
				$(Game._tContainer).html('Enter what-?');
			}
		})
	},
	stopMovement: function(e) {
		
		Crafty.audio.play("blocked",1,0.25);
		this._speed = 0;
		if (this._movement) {
			this.x -= this._movement.x;
			this.y -= this._movement.y;
			var tile = e[0].obj;
		}
	}
});

Crafty.c('MapTile', {
	init: function() {
		this.requires('MouseHighlighter');
	}
});
