Crafty.scene('Game', function() {
	Crafty.viewport.init(352,352); //main window size
	
	map = Crafty.e("TiledLevel").tiledLevel("maps/start.json");
	map.bind("TiledLevelLoaded", Game.begin );
});

Crafty.scene('Britain',function() {
	Crafty.audio.stop('journey');
	Crafty.audio.play('brit',-1);
	var mapBritain = Crafty.e("TiledLevel").tiledLevel("maps/britain.json");
	mapBritain.bind("TiledLevelLoaded", function() { 
		Crafty('PlayerCharacter').attr({x:128,y:128, z: 1000});
		Crafty.viewport.centerOn(Crafty('PlayerCharacter'));
	});
})

Crafty.scene("Introduction",function(){ 	
	
	Crafty.audio.play("theme",-1);	
	var ent = Crafty.e("2D, DOM, Image,Mouse").image("assets/intro.png")
	.bind('Click', function () {  
		Crafty.audio.remove("theme");
		Crafty.scene("Game") 
	}) 
});
// Loading scene
// -------------
// Handles the loading of binary assets such as images and audio files
Crafty.scene('Loading', function(){
  // Draw some text for the player to see in case the file
  //  takes a noticeable amount of time to load
  Crafty.e('2D, DOM, Text')
    .text('Loading...')
    .attr({ x: 0, y: Game.height()/2 - 24, w: Game.width() })
    .css($text_css);

  // Load our sprite map image
  Crafty.load(['assets/u5_tileset.png','assets/audio/theme.mp3','assets/audio/journey.mp3','assets/audio/walk.mp3','assets/audio/brit.mp3'], function(){
    // Once the image is loaded...
Crafty.audio.add("theme", "assets/audio/theme.mp3");
Crafty.audio.add("journey", "assets/audio/journey2.mp3");
Crafty.audio.add("blocked", "assets/audio/walk.mp3");
Crafty.audio.add("brit", "assets/audio/brit.mp3");
    // Define the individual sprites in the image
    // Each one (spr_tree, etc.) becomes a component
    // These components' names are prefixed with "spr_"
    //  to remind us that they simply cause the entity
    //  to be drawn with a certain sprite
	
 Crafty.sprite(32, 'assets/u5_tileset.png', {
      spr_tree:    [0, 0],
      spr_bush:    [1, 0],
      spr_village: [26, 8],
      spr_player:  [14, 10]
    });

    // Now that our sprites are ready to draw, start the game
    Crafty.scene("Introduction");
	//Crafty.scene('Game');
  })
});